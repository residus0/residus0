# Residus 0

En la nostra documentació podràs trobar com fer la instal·lació per poder utilitzar la nostra web.

Versió: **v1.0**
Autors: **Adrián Mellado i Bàrbara Ibáñez**

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licencia Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licencia Creative Commons Atribución-NoComercial-CompartirIgual 4.0 Internacional</a>.

Donem les gràcies a totes les persones que han fet possible dur a terme aquest projecte, i esperem que el disfruteu igual que nosaltres l'hem gaudit amb els bons i els moments amb més dificultats.

:tw-1f4bb:  con :tw-1f496: por Residus0 :tw-1f600:
