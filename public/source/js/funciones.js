// FIREBASE SCRIPTS
// Inicialización a Firebase
var config = {
    apiKey: "AIzaSyDL2NmNIoIHBTpJP4orF1oIFgIb5rUwbDc",
    authDomain: "residus0-d80b4.firebaseapp.com",
    databaseURL: "https://residus0-d80b4.firebaseio.com",
    projectId: "residus0-d80b4",
    storageBucket: "residus0-d80b4.appspot.com",
    messagingSenderId: "874733265821"
};

firebase.initializeApp(config);
db = firebase.firestore();


function dataActual() {
    var d = new Date();
    var mes = d.getMonth() + 1;
    var dia = d.getDate();
    var dataActual = d.getFullYear() + '-' +
        (mes < 10 ? '0' : '') + mes + '-' +
        (dia < 10 ? '0' : '') + dia;
    console.log("Data Actual " + dataActual);
    return dataActual;
}

function logOutUser() {
    firebase.auth().signOut().then(function () {
        $(location).attr('href', '/');
    }).catch(function (error) {
        console.log("Error al logOut");
    });
}

$(document).ready(function () {

    firebase.auth().onAuthStateChanged(function (user) {
        console.log("USER:" + user);
        if (!user) {
            $(location).attr('href', '/');
        }
        var user = firebase.auth().currentUser;
        console.log("TAULER: " + user);
    });

    $('.logOutButt').click(function () {
        console.log("LogOut");
        logOutUser();
    })

});